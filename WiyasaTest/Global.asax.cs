﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using AutoMapper.Configuration;
using BLL.Company.Dto;
using BLL.Company.MapperProfile;
using WiyasaTest.App_Start;
using WiyasaTest.Models;

namespace WiyasaTest
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            //mapper
            //RegisterMapper();
            AutoMapperConfiguration.Configure();
        }

        private void RegisterMapper()
        {
            Mapper.Initialize(map =>
            {
                #region BLL
                //CompanyMapper.
                map.AddProfile<CompanyMapperProfile>();

                #endregion

                #region View Model
                map.CreateMap<CompanyDto, CompanyItem>().ReverseMap();
                map.CreateMap<CompanyDetailDto, CompanyDetail>().ReverseMap();
                #endregion

                Mapper.AssertConfigurationIsValid();
            });
        }

        public class AutoMapperConfiguration
        {
            public static void Configure()
            {
                Mapper.Initialize(x =>
                {
                    x.AddProfile<CompanyMapperProfile>();
                    x.CreateMap<CompanyDto, CompanyItem>().ReverseMap();
                });

                Mapper.Configuration.AssertConfigurationIsValid();
            }
        }
    }
}
