﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WiyasaTest.Models
{
    public class CompanyViewModel
    {
        public CompanyViewModel()
        {
            Details = new List<CompanyItem>();
            Filter = new SearchView();
            ItemEdit = new CompanyDetail();
        }
        public SearchView Filter { get; set; }
        public List<CompanyItem> Details { get; set; }

        public CompanyDetail ItemEdit { get; set; }

        public int TotalData { get; set; }
        public int TotalDataPerPage { get; set; }

        public int CurrentPage { get; set; }

        public string Error { get; set; }

    }

    public class SearchView
    {
        
        public string CMGUnmaskedName { get; set; }
        public string ClientTier { get; set; }
        
        public string CMGSegmentName { get; set; }
        public string GlobalControlPoint { get; set; }
        public decimal Revenue { get; set; }
        public decimal Deposits { get; set; }
        public decimal Roe { get; set; }
    }


    public class CompanyDetail
    {
        public string CMGUnmaskedID { get; set; }
        public string CMGUnmaskedName { get; set; }
        public string ClientTier { get; set; }
        public string GCPStream { get; set; }
        public string GCPBusiness { get; set; }
        public string CMGGlobalBU { get; set; }
        public string CMGSegmentName { get; set; }
        public string GlobalControlPoint { get; set; }
        public string GCPGeography { get; set; }
        public string GlobalRelationshipManagerName { get; set; }

        public decimal REVENUE_FY14 { get; set; }
        public decimal REVENYE_FY15X { get; set; }
        public decimal Deposits_EOP_FY14 { get; set; }
        public decimal Deposits_EOP_FY15x { get; set; }
        public decimal TotalLimits_EOP_FY14 { get; set; }
        public decimal TotalLimits_EOP_FY15YTD { get; set; }
        public decimal TotalLimits_EOP_FY15x { get; set; }
        public decimal RWAFY15 { get; set; }
        public decimal RWA_FY14 { get; set; }
        public decimal REV_RWA_FY14 { get; set; }
        public decimal REV_RWA_FY15 { get; set; }
        public decimal NPAT_AllocEq_FY14 { get; set; }
        public decimal NPAT_AllocEq_FY15X { get; set; }
        public decimal RegulatoryCapital_AVG_FY15 { get; set; }
        public decimal RegulatoryCapital_AVG_FY14 { get; set; }
        public decimal ROE_FY14 { get; set; }
        public decimal ROE_FY15 { get; set; }
    }

    public class CompanyItem
    {
        public string CMGUnmaskedID { get; set; }
        public string CMGUnmaskedName { get; set; }
        public string ClientTier { get; set; }
        public string GCPStream { get; set; }
        public string GCPBusiness { get; set; }
        public string CMGGlobalBU { get; set; }
        public string CMGSegmentName { get; set; }
        public string GlobalControlPoint { get; set; }
        public string GCPGeography { get; set; }
        public string GlobalRelationshipManagerName { get; set; }

        public string REVENUE_FY14 { get; set; }
        public string REVENYE_FY15X { get; set; }
        public string Deposits_EOP_FY14 { get; set; }
        public string Deposits_EOP_FY15x { get; set; }
        public string TotalLimits_EOP_FY14 { get; set; }
        public string TotalLimits_EOP_FY15YTD { get; set; }
        public string TotalLimits_EOP_FY15x { get; set; }
        public string RWAFY15 { get; set; }
        public string RWA_FY14 { get; set; }
        public string REV_RWA_FY14 { get; set; }
        public string REV_RWA_FY15 { get; set; }
        public string NPAT_AllocEq_FY14 { get; set; }
        public string NPAT_AllocEq_FY15X { get; set; }
        public string RegulatoryCapital_AVG_FY15 { get; set; }
        public string RegulatoryCapital_AVG_FY14 { get; set; }
        public string ROE_FY14 { get; set; }
        public string ROE_FY15 { get; set; }
    }
}