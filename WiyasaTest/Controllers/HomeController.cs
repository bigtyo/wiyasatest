﻿using System.Configuration;
using System.Web.Routing;
using AutoMapper;
using BLL.Company.Dto;
using BLL.Company.Logics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WiyasaTest.Models;

namespace WiyasaTest.Controllers
{
    public class HomeController : Controller
    {
        private CompanyBLL _companyBLL;
        private List<CompanyDto> _cachedData;
        private string _cacheKey = "cachedData";

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            var pathFile = Server.MapPath(ConfigurationManager.AppSettings["DataFile"]);
            _companyBLL = new CompanyBLL(pathFile);
            
        }

        public ActionResult Index()
        {
            var model = new CompanyViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(CompanyViewModel model)
        {
            var datatoSave = Mapper.Map<CompanyDetailDto>(model.ItemEdit);

            try
            {
                var cache = (List<CompanyDto>)HttpContext.Cache[_cacheKey];

                _companyBLL.SaveData(datatoSave, cache);

                //refresh cache
                cache = _companyBLL.GetData();
                HttpContext.Cache[_cacheKey] = cache;
                
            }
            catch (Exception ex)
            {
                model.Error = ex.Message;
            }
            

            return View(model);
        }

        [HttpPost]
        public JsonResult SearchCompanyAjax(DTParameters<CompanyViewModel> param)
        {
            var model = param.ExtraFilter;

            var data = model != null ? SearchDataCompany(model.Filter) : SearchDataCompany();
            DTResult<CompanyItem> result = new DTResult<CompanyItem>();
            result.draw = param.Draw;
            result.recordsFiltered = data.Count;
            result.recordsTotal = data.Count;
           
            data = data.Skip(param.Start).Take(param.Length).ToList();

           
            result.data = data;

            return Json(result);

        }

        [HttpPost]
        public JsonResult GetCompanyById(string id)
        {
            
            var cachedData = (List<CompanyDto>)HttpContext.Cache[_cacheKey];
            CompanyDetailDto retData = _companyBLL.GetDataById(id, cachedData);
            var data = Mapper.Map<CompanyDetailDto, CompanyDetail>(retData);

            return Json(data);
        }

       

        private List<CompanyItem> SearchDataCompany(SearchView filter = null)
        {
            List<CompanyItem> data = new List<CompanyItem>();

            var retData = (List<CompanyDto>)HttpContext.Cache[_cacheKey]; 
            if (retData == null)
            {
                retData = _companyBLL.GetData();
                HttpContext.Cache[_cacheKey] = retData;
            }
            
            data = Mapper.Map<List<CompanyDto>, List<CompanyItem>>(retData);

            if (filter != null)
            {
                if (!string.IsNullOrEmpty(filter.CMGUnmaskedName))
                {
                    data = data.Where(x => x.CMGUnmaskedName.ToLower().Contains(filter.CMGUnmaskedName.ToLower())).ToList();
                }

                if (!string.IsNullOrEmpty(filter.CMGSegmentName))
                {
                    data = data.Where(x => x.CMGSegmentName.ToLower().Contains(filter.CMGSegmentName.ToLower())).ToList();
                }


                if (!string.IsNullOrEmpty(filter.ClientTier))
                {
                    data = data.Where(x => x.ClientTier.ToLower().Contains(filter.ClientTier.ToLower())).ToList();
                }


                if (!string.IsNullOrEmpty(filter.GlobalControlPoint))
                {
                    data = data.Where(x => x.GlobalControlPoint.ToLower().Contains(filter.GlobalControlPoint.ToLower())).ToList();
                }

                if (filter.Roe != 0)
                {
                    data = data.Where(x => x.ROE_FY14 == filter.Roe.ToString("N2") + "%" || x.ROE_FY15 == filter.Roe.ToString("N2") + "%").ToList();
                }

                if (filter.Deposits != 0)
                {
                    data = data.Where(x => x.Deposits_EOP_FY14 == filter.Deposits.ToString("N2") || x.Deposits_EOP_FY15x == filter.Deposits.ToString("N2")).ToList();
                }

                if (filter.Revenue != 0)
                {
                    data = data.Where(x => x.REVENUE_FY14 == filter.Revenue.ToString("N2") || x.REVENYE_FY15X == filter.Revenue.ToString("N2")).ToList();
                }
            }

            return data;
        } 
    }
}