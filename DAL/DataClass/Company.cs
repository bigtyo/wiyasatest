﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DataClass
{
    public class Company
    {
        public string CMGUnmaskedID { get; set; }
        public string CMGUnmaskedName { get; set; }
        public string ClientTier { get; set; }
        public string GCPStream { get; set; }
        public string GCPBusiness { get; set; }
        public string CMGGlobalBU { get; set; }
        public string CMGSegmentName { get; set; }
        public string GlobalControlPoint { get; set; }
        public string GCPGeography { get; set; }
        public string GlobalRelationshipManagerName { get; set; }

        public string REVENUE_FY14 { get; set; }
        public string REVENYE_FY15X { get; set; }
        public string Deposits_EOP_FY14 { get; set; }
        public string Deposits_EOP_FY15x { get; set; }
        public string TotalLimits_EOP_FY14 { get; set; }
        public string TotalLimits_EOP_FY15YTD { get; set; }
        public string TotalLimits_EOP_FY15x { get; set; }
        public string RWAFY15 { get; set; }
        public string RWA_FY14 { get; set; }
        public string REV_RWA_FY14 { get; set; }
        public string REV_RWA_FY15 { get; set; }
        public string NPAT_AllocEq_FY14 { get; set; }
        public string NPAT_AllocEq_FY15X { get; set; }
        public string RegulatoryCapital_AVG_FY15 { get; set; }
        public string RegulatoryCapital_AVG_FY14 { get; set; }
        public string ROE_FY14 { get; set; }
        public string ROE_FY15 { get; set; }
    }
}
