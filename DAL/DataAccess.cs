﻿
using DAL.DataClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Reflection;
using System.Linq;


namespace DAL
{
    public class DataAccess
    {
        private string _connectionString;
        private string fileName;
        private string completePath;
        public DataAccess(string filePath)
        {
            //var filePath = ConfigurationManager.AppSettings["DataFile"];
            fileName = Path.GetFileName(filePath);
            //var filename = filePath.Split('\\')[filePath.Split('\\').Length - 1];
            //string sql = @"SELECT * FROM [" + filename + "] WHERE Column1 LIKE 'blah%'";
            _connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path.GetDirectoryName(filePath) +
                      ";Extended Properties=\"Text;HDR=YES\"";
            
        }

        public List<Company> GetData()
        {
            using (var conn = new OleDbConnection(_connectionString))
            {
                conn.Open();
                var query = "SELECT * FROM [" + Path.GetFileName(fileName) + "]";
                using (var adapter = new OleDbDataAdapter(query, conn))
                {
                    var ds = new DataSet("CSV File");
                    adapter.Fill(ds);
                    var dt = ds.Tables[0];
                    return dt.ToList<Company>();
                    
                }
            }
        }



        public void SaveToFile(List<Company> datatoSave)
        {
            try
            {
                WriteCSV<Company>(datatoSave, completePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        private void WriteCSV<T>(IEnumerable<T> items, string path)
        {
          Type itemType = typeof(T);
          var props = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

          using (var writer = new StreamWriter(path))
          {
            writer.WriteLine(string.Join(", ", props.Select(p => p.Name)));

            foreach (var item in items)
            {
              writer.WriteLine(string.Join(", ", props.Select(p => p.GetValue(item, null))));
            }
          }
        }
    }
}
