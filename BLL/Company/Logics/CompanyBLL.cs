﻿using System.Linq;
using System.Runtime.InteropServices;
using AutoMapper;
using BLL.Company.Dto;
using System;
using System.Collections.Generic;
using DAL;

namespace BLL.Company.Logics
{
    
    public class CompanyBLL
    {
        private List<DAL.DataClass.Company> _dataCompany;
        private DataAccess _da;
        public CompanyBLL(string filePath)
        {
            _da = new DataAccess(filePath);
            
        }

        public List<CompanyDto> GetData()
        {
            _dataCompany = _da.GetData();
            var retData = Mapper.Map<List<DAL.DataClass.Company>, List<CompanyDto>>(_dataCompany);
            //var retData = new List<CompanyDto>();
            //foreach (var data in _dataCompany)
            //{
                
            //}
            return retData;
            //return new List<CompanyDto>();
        }

        private decimal StringToDecimal(string val)
        {
            var tempString = val;

            if (val.Contains("#DIV/0!"))
            {
                return 0;
            }

            if (val.Trim() == "-")
            {
                return 0;
            }

            tempString = val.Replace("%", "");

            tempString = tempString.Replace(",", "");

            return decimal.Parse(tempString);
        }

        public CompanyDetailDto GetDataById(string id,List<CompanyDto> cachedData)
        {
            if (cachedData == null)
            {
                cachedData = GetData();
            }
            var data = cachedData.Where(x => x.CMGUnmaskedID == id).FirstOrDefault();
            if (data != null)
            {

                var detailData = ConvertToDetails(data);
                return detailData;
            }
            else
            {
                return null;
            }
            
        }

        private CompanyDetailDto ConvertToDetails(CompanyDto data)
        {
            return new CompanyDetailDto()
            {
                CMGGlobalBU = data.CMGGlobalBU,
                CMGSegmentName = data.CMGSegmentName,
                CMGUnmaskedID = data.CMGUnmaskedID,
                CMGUnmaskedName = data.CMGUnmaskedName,
                ClientTier = data.ClientTier,
                Deposits_EOP_FY14 = ManualResolve(data.Deposits_EOP_FY14),
                Deposits_EOP_FY15x = ManualResolve(data.Deposits_EOP_FY15x),
                GCPBusiness = data.GCPBusiness,
                GCPGeography = data.GCPGeography,
                GCPStream = data.GCPStream,
                GlobalControlPoint = data.GlobalControlPoint,
                GlobalRelationshipManagerName = data.GlobalRelationshipManagerName,
                NPAT_AllocEq_FY14 = ManualResolve(data.NPAT_AllocEq_FY14),
                NPAT_AllocEq_FY15X = ManualResolve(data.NPAT_AllocEq_FY15X),
                REVENUE_FY14 = ManualResolve(data.REVENUE_FY14),
                REVENYE_FY15X = ManualResolve(data.REVENYE_FY15X),
                REV_RWA_FY14 = ManualResolve(data.REV_RWA_FY14),
                REV_RWA_FY15 = ManualResolve(data.REV_RWA_FY15),
                ROE_FY14 = ManualResolve(data.ROE_FY14),
                ROE_FY15 = ManualResolve(data.ROE_FY15),
                RWAFY15 = ManualResolve(data.RWAFY15),
                RWA_FY14 = ManualResolve(data.RWA_FY14),
                RegulatoryCapital_AVG_FY14 = ManualResolve(data.RegulatoryCapital_AVG_FY14),
                RegulatoryCapital_AVG_FY15 = ManualResolve(data.RegulatoryCapital_AVG_FY15),
                TotalLimits_EOP_FY14 = ManualResolve(data.TotalLimits_EOP_FY14),
                TotalLimits_EOP_FY15YTD = ManualResolve(data.TotalLimits_EOP_FY15YTD),
                TotalLimits_EOP_FY15x = ManualResolve(data.TotalLimits_EOP_FY15x)
            };
        }

        private decimal ManualResolve(string sourceMember)
        {
            var tempString = sourceMember;
            if (string.IsNullOrEmpty(sourceMember))
            {
                return 0;
            }
            if (sourceMember.Contains("#DIV/0!"))
            {
                return 0;
            }

            if (sourceMember.Trim() == "-")
            {
                return 0;
            }

            tempString = sourceMember.Replace("%", "");

            tempString = tempString.Replace(",", "");

            //return decimal.Parse(tempString);
            var retNumber = (decimal)0;
            if (decimal.TryParse(tempString, out retNumber))
            {
                return retNumber;
            }
            else
            {
                return 0;
            }
        }

        public void SaveData(CompanyDetailDto datatoSave,List<CompanyDto> cachedData)
        {
            var dataList = cachedData;
            if (dataList == null)
            {
                dataList = GetData();
            }

            var data = dataList.FirstOrDefault(x=> x.CMGUnmaskedID == datatoSave.CMGUnmaskedID);
            if (data != null)
            {
                
                data.CMGUnmaskedName = datatoSave.CMGUnmaskedName;
                data.ClientTier = datatoSave.ClientTier;
                data.GCPStream = datatoSave.GCPStream;
                data.GCPBusiness = datatoSave.GCPBusiness;
                data.CMGGlobalBU = datatoSave.CMGGlobalBU;
                data.CMGSegmentName = datatoSave.CMGSegmentName;
                data.GlobalControlPoint = datatoSave.GlobalControlPoint;
                data.GCPGeography = datatoSave.GCPGeography;
                data.GlobalRelationshipManagerName = datatoSave.GlobalRelationshipManagerName;
                data.REVENUE_FY14 = datatoSave.REVENUE_FY14.ToString("N");
                data.REVENYE_FY15X = datatoSave.REVENUE_FY14.ToString("N");
                data.Deposits_EOP_FY14 = datatoSave.Deposits_EOP_FY14.ToString("N");
                data.Deposits_EOP_FY15x = datatoSave.Deposits_EOP_FY15x.ToString("N");
                data.TotalLimits_EOP_FY14 = datatoSave.TotalLimits_EOP_FY14.ToString("N");
                data.TotalLimits_EOP_FY15YTD = datatoSave.TotalLimits_EOP_FY15YTD.ToString("N");
                data.TotalLimits_EOP_FY15x = datatoSave.TotalLimits_EOP_FY15x.ToString("N");
                data.RWAFY15 = datatoSave.RWAFY15.ToString("N");
                data.RWA_FY14 = datatoSave.RWA_FY14.ToString("N");
                data.REV_RWA_FY14 = datatoSave.REV_RWA_FY14.ToString("P");
                data.REV_RWA_FY15 = datatoSave.REV_RWA_FY15.ToString("P");
                data.NPAT_AllocEq_FY14 = datatoSave.NPAT_AllocEq_FY14.ToString("N");
                data.NPAT_AllocEq_FY15X = datatoSave.NPAT_AllocEq_FY15X.ToString("N");
                data.RegulatoryCapital_AVG_FY15 = datatoSave.RegulatoryCapital_AVG_FY15.ToString("N");
                data.RegulatoryCapital_AVG_FY14 = datatoSave.RegulatoryCapital_AVG_FY14.ToString("N");
                data.ROE_FY14 = datatoSave.ROE_FY14.ToString("P");
                data.ROE_FY15 = datatoSave.ROE_FY15.ToString("P");



            }
            else
            {
                data = new CompanyDto();
                data.CMGUnmaskedID = datatoSave.CMGUnmaskedID;
                data.CMGUnmaskedName = datatoSave.CMGUnmaskedName;
                data.ClientTier = datatoSave.ClientTier;
                data.GCPStream = datatoSave.GCPStream;
                data.GCPBusiness = datatoSave.GCPBusiness;
                data.CMGGlobalBU = datatoSave.CMGGlobalBU;
                data.CMGSegmentName = datatoSave.CMGSegmentName;
                data.GlobalControlPoint = datatoSave.GlobalControlPoint;
                data.GCPGeography = datatoSave.GCPGeography;
                data.GlobalRelationshipManagerName = datatoSave.GlobalRelationshipManagerName;
                data.REVENUE_FY14 = datatoSave.REVENUE_FY14.ToString("N");
                data.REVENYE_FY15X = datatoSave.REVENUE_FY14.ToString("N");
                data.Deposits_EOP_FY14 = datatoSave.Deposits_EOP_FY14.ToString("N");
                data.Deposits_EOP_FY15x = datatoSave.Deposits_EOP_FY15x.ToString("N");
                data.TotalLimits_EOP_FY14 = datatoSave.TotalLimits_EOP_FY14.ToString("N");
                data.TotalLimits_EOP_FY15YTD = datatoSave.TotalLimits_EOP_FY15YTD.ToString("N");
                data.TotalLimits_EOP_FY15x = datatoSave.TotalLimits_EOP_FY15x.ToString("N");
                data.RWAFY15 = datatoSave.RWAFY15.ToString("N");
                data.RWA_FY14 = datatoSave.RWA_FY14.ToString("N");
                data.REV_RWA_FY14 = datatoSave.REV_RWA_FY14.ToString("P");
                data.REV_RWA_FY15 = datatoSave.REV_RWA_FY15.ToString("P");
                data.NPAT_AllocEq_FY14 = datatoSave.NPAT_AllocEq_FY14.ToString("N");
                data.NPAT_AllocEq_FY15X = datatoSave.NPAT_AllocEq_FY15X.ToString("N");
                data.RegulatoryCapital_AVG_FY15 = datatoSave.RegulatoryCapital_AVG_FY15.ToString("N");
                data.RegulatoryCapital_AVG_FY14 = datatoSave.RegulatoryCapital_AVG_FY14.ToString("N");
                data.ROE_FY14 = datatoSave.ROE_FY14.ToString("P");
                data.ROE_FY15 = datatoSave.ROE_FY15.ToString("P");

                dataList.Add(data);
            }

            SaveToFile(dataList);
        }

        public void SaveToFile(List<CompanyDto> dataList)
        {
            try
            {
                var datatoSave = Mapper.Map<List<CompanyDto>, List<DAL.DataClass.Company>>(dataList);
                _da.SaveToFile(datatoSave);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }


    }
}
