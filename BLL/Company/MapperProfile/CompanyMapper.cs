﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Company.Dto;

namespace BLL.Company.MapperProfile
{
    public class CompanyMapperProfile : Profile
    {
        public void Initialize()
        {
            CreateMap<DAL.DataClass.Company, CompanyDto>()
                
                //.ForMember(dest => dest.REVENUE_FY14, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.REVENUE_FY14))
                //.ForMember(dest => dest.REVENYE_FY15X, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.REVENYE_FY15X))
                //.ForMember(dest => dest.Deposits_EOP_FY14, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.Deposits_EOP_FY14))
                //.ForMember(dest => dest.Deposits_EOP_FY15x, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.Deposits_EOP_FY15x))
                //.ForMember(dest => dest.TotalLimits_EOP_FY14, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.TotalLimits_EOP_FY14))
                //.ForMember(dest => dest.TotalLimits_EOP_FY15YTD, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.TotalLimits_EOP_FY15YTD))
                //.ForMember(dest => dest.TotalLimits_EOP_FY15x, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.TotalLimits_EOP_FY15x))
                //.ForMember(dest => dest.RWAFY15, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.RWAFY15))
                //.ForMember(dest => dest.RWA_FY14, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.RWA_FY14))
                //.ForMember(dest => dest.REV_RWA_FY14, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.REV_RWA_FY14))
                //.ForMember(dest => dest.REV_RWA_FY15, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.REV_RWA_FY15))
                //.ForMember(dest => dest.NPAT_AllocEq_FY14, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.NPAT_AllocEq_FY14))
                //.ForMember(dest => dest.NPAT_AllocEq_FY15X, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.NPAT_AllocEq_FY15X))
                //.ForMember(dest => dest.RegulatoryCapital_AVG_FY15, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.RegulatoryCapital_AVG_FY15))
                //.ForMember(dest => dest.RegulatoryCapital_AVG_FY14, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.RegulatoryCapital_AVG_FY14))
                //.ForMember(dest => dest.ROE_FY14, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.ROE_FY14))
                //.ForMember(dest => dest.ROE_FY15, opt => opt.ResolveUsing<StringToDecimalConverter, string>(y => y.ROE_FY15))
                
                ;
                    
            CreateMap<CompanyDto, DAL.DataClass.Company>()
                //.ForMember(dest => dest.REVENUE_FY14, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.REVENUE_FY14))
                //.ForMember(dest => dest.REVENYE_FY15X, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.REVENYE_FY15X))
                //.ForMember(dest => dest.Deposits_EOP_FY14, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.Deposits_EOP_FY14))
                //.ForMember(dest => dest.Deposits_EOP_FY15x, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.Deposits_EOP_FY15x))
                //.ForMember(dest => dest.TotalLimits_EOP_FY14, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.TotalLimits_EOP_FY14))
                //.ForMember(dest => dest.TotalLimits_EOP_FY15YTD, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.TotalLimits_EOP_FY15YTD))
                //.ForMember(dest => dest.TotalLimits_EOP_FY15x, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.TotalLimits_EOP_FY15x))
                //.ForMember(dest => dest.TotalLimits_EOP_FY15x, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.RWAFY15))
                //.ForMember(dest => dest.RWA_FY14, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.RWA_FY14))
                //.ForMember(dest => dest.REV_RWA_FY14, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.REV_RWA_FY14))
                //.ForMember(dest => dest.REV_RWA_FY15, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.REV_RWA_FY15))
                //.ForMember(dest => dest.NPAT_AllocEq_FY14, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.NPAT_AllocEq_FY14))
                //.ForMember(dest => dest.NPAT_AllocEq_FY15X, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.NPAT_AllocEq_FY15X))
                //.ForMember(dest => dest.RegulatoryCapital_AVG_FY15, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.RegulatoryCapital_AVG_FY15))
                //.ForMember(dest => dest.RegulatoryCapital_AVG_FY14, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.RegulatoryCapital_AVG_FY14))
                //.ForMember(dest => dest.ROE_FY14, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.ROE_FY14))
                //.ForMember(dest => dest.ROE_FY15, opt => opt.ResolveUsing<DecimalToStringConverter, decimal>(y => y.ROE_FY15))
                ;
        }

        public class DecimalToStringConverter : IMemberValueResolver<object, object, decimal, string>
        {

            //public decimal Convert(string source, decimal destination, ResolutionContext context)
            //{
            //    var tempString = source;

            //    if (source.Contains("#DIV/0!"))
            //    {
            //        return 0;
            //    }

            //    tempString = source.Replace("%","");

            //    tempString = tempString.Replace(",", "");

            //    return decimal.Parse(tempString);
            //}

            

            public string Resolve(object source, object destination, decimal sourceMember, string destMember, ResolutionContext context)
            {
                var tempString = destMember;

                if (sourceMember == 0)
                {
                    return "-";
                }
                else
                {
                    return sourceMember.ToString("N2");
                }
            }
        }

        public class StringToDecimalConverter : IMemberValueResolver<object,object,string, decimal>
        {
            
            //public decimal Convert(string source, decimal destination, ResolutionContext context)
            //{
            //    var tempString = source;

            //    if (source.Contains("#DIV/0!"))
            //    {
            //        return 0;
            //    }

            //    tempString = source.Replace("%","");

            //    tempString = tempString.Replace(",", "");
                
            //    return decimal.Parse(tempString);
            //}

            public decimal Resolve(object source, object destination, string sourceMember, decimal destMember, ResolutionContext context)
            {
                var tempString = sourceMember;

                if (sourceMember.Contains("#DIV/0!"))
                {
                    return 0;
                }

                if (sourceMember.Trim() == "-")
                {
                    return 0;
                }

                tempString = sourceMember.Replace("%", "");

                tempString = tempString.Replace(",", "");

                //return decimal.Parse(tempString);
                var retNumber = (decimal)0;
                if (decimal.TryParse(tempString, out retNumber))
                {
                    return retNumber;
                }
                else
                {
                    return 0;
                }
            }
        }


        public class DecimalToStringPercent : ITypeConverter<decimal, string>
        {
            public string Convert(decimal source, string destination, ResolutionContext context)
            {
                return source.ToString("P");
            }
        }
    }
}
